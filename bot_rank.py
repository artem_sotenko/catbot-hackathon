__author__ = 'kamalov mikhail <mkamalovv@gmail.com>'
import numpy as np

from sklearn.neighbors import NearestNeighbors as nn

class Bot_Rank:

    def __init__(self, near_neybors=2):
        self.nn = near_neybors

    def _preprocessing(self, X):
        n = X.shape[0]
        s = (n, n)
        A = np.zeros(s)
        b = np.ones(n) / n
        neigh = nn(n_neighbors=self.nn, algorithm='kd_tree')
        neigh.fit(X)
        #create graph
        for i in range(n):
            current = X[i]
            list_neybors = neigh.kneighbors(current)[1][0]
            for j in list_neybors:
                A[i][j] = 1
                A[j][i] = 1

        for i in range(n):
            sum_P = np.sum(A[i])
            A[i] = A[i] / sum_P
        return A, b, n

    def predict(self, X):
        print X
        A, b, n = self._preprocessing(X)
        # current count page rank
        I = np.eye(n)
        real = (1 - 0.85) * (np.linalg.inv(I - 0.85 * A.T)).dot(b)
        sort_ind = np.argsort(real)
        return sort_ind[::-1][:n]