import requests

# o = MuzisAPI()
# print(o.search({'q_track': 'death'})['songs'][0])
# print(o.search({'q_performer': 'metallica'})['performers'][0])
# print(o.stream_from_obj({'id': 5145, 'type': 2})['songs'][0])
# print(o.stream_from_lyrics({'lyrics': love:80,yes:30,hous', 'operator': 'AND'})['songs'][0])
# print(o.similar_performers({'performer_id': '16270'})['performers'][1])

class MuzisAPI:
    api_url = 'http://muzis.ru'

    def make_request(self, url, params):
        r = requests.post(self.api_url + '/api/' + url + '.api', data = params)
        return r.json()

    def search(self,params):
        return self.make_request('search',params)

    def stream_from_obj(self,params):
        return self.make_request('stream_from_obj',params)

    def stream_from_lyrics(self,params):
        return self.make_request('stream_from_lyrics',params)

    def stream_from_values(self,params):
        return self.make_request('stream_from_values',params)

    def similar_performers(self,params):
        return self.make_request('similar_performers',params)

    def get_songs_by_performer(self,params):
        return self.make_request('get_songs_by_performer',params)