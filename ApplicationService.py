import os
import logging
import json

from flask import Flask, jsonify, request, send_from_directory
import os.path as pth
from PartyBot import PartyBot
import playlists

app = Flask(__name__, instance_relative_config=True)
commandProcessor = None
notDefinedCommand = "Command not defined"
configPath = pth.join(pth.dirname(pth.abspath(__file__)),'analyticsService.cfg')
tnkPath = pth.join(pth.dirname(pth.abspath(__file__)),'tnk.txt')
logger = None
app.config.from_pyfile(configPath, silent=True)
app.config.from_pyfile(tnkPath, silent=True)
bot = PartyBot(app.config['TNK'])

def init():

    initLogging(app.config['LOGGING_LEVEL'])
#    app.errorhandler(Exception)(onError)

    print('The bot is alive',bot.bot.getMe())
    # app.errorhandler(404)(on404)

def run():
    app.run(host = app.config['HOST'], debug=app.config['DEBUG'], port=int(os.environ.get("PORT",app.config['PORT'])))

def onError(e):
    return jsonify(dict(error=e.msg)), 400

@app.route('/<path:path>')
def send_static(path):
    return send_from_directory('static', path)

def initLogging(level):
    logFormatStr = '%(levelname)s in %(module)s [%(pathname)s:%(lineno)d]:\n%(message)s'
    logging.basicConfig(format = logFormatStr, filename = "application.log")
    formatter = logging.Formatter(logFormatStr,'%m-%d %H:%M:%S')
    logger.setLevel(logging._levelNames[level])
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

@app.route('/get_next_track/<chat_id>')
def get_next_track(chat_id):
    playlist = playlists.get_playlist(chat_id)

    result = jsonify({'chat_id': playlist['chat_id'], 'playlist': playlist['playlist']})

    playlist['playlist'] = playlist['playlist'][1:]
    playlists.update_playlist(playlist['chat_id'], json.dumps(playlist['playlist']))

    return result
